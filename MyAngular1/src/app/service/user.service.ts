import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../model/login';
import { LoginResp } from '../model/login-resp';
import { User } from '../model/user';
import { UserFilterResp } from '../model/user-filter-resp';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  createUser(user: User) {
    return this.http.post(`${environment.apiHost}/user`, user);
  }

  login (login: Login): Observable<LoginResp> {
    return this.http.post<LoginResp>(`${environment.apiHost}/user/login`, login);
  }

  filter(_filter: any) {
    return this.http.get<UserFilterResp[]>(`${environment.apiHost}/user`);
  }

}
