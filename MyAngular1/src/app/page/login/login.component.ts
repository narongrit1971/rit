import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private fb: FormBuilder, 
    private userService: UserService,
    private router: Router
    ) {}

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    pwd: ['', [Validators.required, Validators.maxLength(15)]],
  });

  ngOnInit(): void {}

  onLoginSubmit() {
    if (this.loginForm.valid) {
      this.userService.login(this.loginForm.value).subscribe((resp) => {
        if (resp.success) {
          console.log('Login succes');
        } else {
          console.log(resp.msgCode);
        }
      });
    } else {
    }
  }

  gotoResisterPage(){
      this.router.navigate(['','register']);
  }
}
