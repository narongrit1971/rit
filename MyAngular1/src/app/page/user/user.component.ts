import { Component, OnInit } from '@angular/core';
import { UserFilterResp } from 'src/app/model/user-filter-resp';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  dataSource: UserFilterResp[] = [];
  displayedColumns: string[] = ['code', 'name', 'email', 'role', 'active'];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.filter({}).subscribe((resp) => {
      this.dataSource = resp;
    });
  }
}
