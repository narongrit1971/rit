export interface UserFilterResp {
    code: string;
    name: string;
    pwd: string;
    email: string;
    active: string;
    role: string;
    
}