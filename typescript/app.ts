/*
interface User{
    code:string;
    name:string;
    age?:number;
    email?:string;//optional
}
*/
import {User} from "./user";

class Greeting{
    name : string;
    constructor(name:string){
        console.log(name)
        this.name=name;
    }
    hello(): void{
        console.log(this.name);
    }

    //say(name:string,age:number): string{
        say(user:User): string{  
        this.hello();
        //let x:string = name + " " + String(age);
        return `${user.code}  ${user.name}`;
    }
}

const g= new Greeting("aaaaaa");
let msg =g.say({code:"xxxx",name:"yyyy"});
console.log(msg);