"use strict";
exports.__esModule = true;
var Greeting = /** @class */ (function () {
    function Greeting(name) {
        console.log(name);
        this.name = name;
    }
    Greeting.prototype.hello = function () {
        console.log(this.name);
    };
    //say(name:string,age:number): string{
    Greeting.prototype.say = function (user) {
        this.hello();
        //let x:string = name + " " + String(age);
        return user.code + "  " + user.name;
    };
    return Greeting;
}());
var g = new Greeting("aaaaaa");
var msg = g.say({ code: "xxxx", name: "yyyy" });
console.log(msg);
