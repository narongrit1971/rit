import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "tblservice_reg"
})
export class User{
    @PrimaryGeneratedColumn({ name:'id'})
    id: number;

    @Column({ name: 'user_id'})
    user_id: number;

    @Column({ name: 'service_id'})
    service_id: number;

    @Column({ name: 'reg_date'})
    reg_date: Date;

    @Column({ name: 'status',length:1})
    status: string;

    @Column({ name: 'reg_time',length:5})
    reg_time: string;

    @Column({ name: 'note',length:300})
    note: string;
}