import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "tblservice"
})
export class Service{
    @PrimaryGeneratedColumn({ name:'id'})
    id: number;

    @Column({ name: 'code',length:5})
    code: string;

    @Column({ name: 'name',length:30})
    name: string;

}