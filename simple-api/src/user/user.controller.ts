import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { pseudoRandomBytes } from 'crypto';
import { LoginDTO } from './login.dto';
import { UserFilter } from './user-filter.dto';
import { UserDTO } from './user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  createUser(@Body() body: UserDTO) {
    return this.userService.createUser(body);
  }
  // http://localhost:3000/user/login
  @Post('login')
  login(@Body() body: LoginDTO) {
    return this.userService
      .login(body)
      .then((resp) => resp)
      .catch((reason) => reason);
  }

  @Get()
  filter(@Query() _filter: UserFilter) {
    // console.log(_filter);
    // const filter = JSON.parse(_filter);
    return this.userService.filter(_filter);
  }
  // http://localhost:3000/user/active/1/Y
  @Put('active/:userId/:active')
  setActive(@Param('userId') userId: number, @Param('active') active: string) {
    return this.userService.setActive(userId, active);
  }

}
