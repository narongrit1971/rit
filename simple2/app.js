

/*แบบที่ 1 connect server
var http = require('http');
http.createServer(function(req,res){
    res.writeHead(200,{'Content-Type':'text/plain'});
    res.end('yes you can\n');
}).listen(8000);
console.log('server run port 8000')
*/
//แบบที่ 2 
//var http = require('http');
//const server = http.createServer().listen(8000);
//
//const serverHandler = function(req,res){
//    res.writeHead(200,{'Content-Type':'text/plain'});
//    res.end('yes you can\n');
//}
//const serverHandler = require("./server");


//server.on('request',serverHandler);
//console.log('server run port 8000')

//Express
var http = require('http');
var express = require("express");
const app = express();
const server = require("http").Server(app);
const user = require("./user");
var cors = require('cors')
//
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(cors());

app.get('/',function(req,res){
    res.end("xxxxxxxx");

});
app.get('/ab?cd',function(req,res){
    res.end("This is a abcd route");

});
app.get('/users/:userId/books/:bookId',function(req,res){
    let userId = req.params.userId;
    let bookId = req.params.bookId;
    res.end("userId= " + userId + " bookId = "+ bookId);

});

app.use('/user',user);

server.listen(8000);
console.log('server run port 8000');
