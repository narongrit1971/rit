var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var mailer = require("nodemailer");
var excel = require("excel4node");

const pool = mysql.createPool({
    host: 'localhost',
    user: 'usr',
    password: 'usr',
    database: 'test_db',
    connectionLimit:10,
    multipleStatements:true,
});
//get ,delete ไม่มี body
router.get('/',function(req,res){
    let user = req.query;
    let sql = `select id,code,name from tbluser
                 where code like ('%${user.code}%')     
                 and name like ('%${user.name}%')       
    `
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    res.end("sql error");
                }else {
                    res.json(result);
                }
            });
        }
    });

});

router.post('/',function(req,res){
    let user = req.body;
    let sql = `insert into tbluser(code,name,pwd,email)
                values('${user.code}','${user.name}','${user.pwd}','${user.email}')
    `;
    pool.getConnection(function(err,connection){
        if(err){
            res.end("error...");    
        }else{
            connection.query(sql,function(err,result){
                connection.release();
                if (err) {
                    res.end("sql error");
                } else {
                    config={
                        "host":"smtp.gmail.com",
                        "port":587,
                        "secure":false,
                        "auth":{
                            "user":"narongrit1971@gmail.com",
                            "pass":"aO10022514"
                        }
                    };
                    let smtpTransport = mailer.createTransport(config);
                    let mail =`{
                            to: '${user.email}',
                            subject:'Hello ',
                            html:'this is a book',
                            
                             }
                           `
                    smtpTransport.sendMail(mail,function(error,response){
                    smtpTransport.close();
                    if (error) {
                        console.log(error);
                    } else {
                        console.log("mail ok");         
                    }
                    });
                    res.end("insert success");
                }
            });
        }
    });
    
    //res.end("create user");
});

router.put('/:id', function(req, res){
    let id = req.params.id;
    let user = req.body;
    let sql = `
        update tbluser 
           set code = '${user.code}',
               name = '${user.name}'
         where id = ${id}
    `;
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    console.log(sql);
                    res.end("sql error");
                }else {
                    res.end("update success");
                }
            });
        }
    });
});



router.delete('/:id',function(req,res){
    //let user = req.body;
    let sql = "delete from tbluser where code='"+ req.params.id +"'";
    pool.getConnection(function(err,connection){
        if(err){
            res.end("error...");    
        }else{
            connection.query(sql,function(err,result){
                connection.release();
                if (err) {
                    res.end("sql error");
                } else {
                    res.end("delete success");
                }
            });
        }
    });
    //res.end("delete user");
});

router.get('/export', function(req, res) {
    let user = req.query;
    let sql = `
        select id, code, name
          from tbluser
         where code like('%${user.code}%')
           and name like('%${user.name}%')
    `
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    res.end("sql error");
                }else {
                    let wb = new excel.Workbook();
                    let ws = wb.addWorksheet("Sheet 1");
                    ws.cell(1, 1).string("ID");
                    ws.cell(1, 2).string("CODE");
                    ws.cell(1, 3).string("NAME");
                    for (let i = 0; i < result.length; i++) {
                        let user =result[i];
                        ws.cell(i+2,1).number(user.id);
                        ws.cell(i+2,2).string(user.code);
                        ws.cell(i+2,3).string(user.name);
                        
                    }
                    //ws.cell(1, 1).string("Hello");
                    res.header("Content-Type", "application/octet-stream");
                    wb.write("user.xlsx", res);
                }
            });
        }
    });
});


module.exports = router;
